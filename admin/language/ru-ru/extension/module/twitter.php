<?php
// Heading
$_['heading_title']    = 'Твиттер';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_screen_name']     = 'Отслеживаемая страница';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';

