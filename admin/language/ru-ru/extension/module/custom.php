<?php
// Heading
$_['heading_title']    = 'Кастомный блок';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_label']      = 'Название блока';
$_['entry_text']       = 'Текст блока';
$_['entry_button']     = 'Текст на кнопке';
$_['entry_link']       = 'Ссылка для кнопки';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';

