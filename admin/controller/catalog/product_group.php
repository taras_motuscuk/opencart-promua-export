<?php

class ControllerCatalogProductGroup extends Controller
{
    private $error = array();

    public function index ()
    {
        $this->load->language('catalog/product_group');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');
        $this->load->model('catalog/product_group');

        $this->getList();
    }

    protected function getList ()
    {

        $url = '';
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/product_group/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['copy'] = $this->url->link('catalog/product_group/copy', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/product_group/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['products'] = array();


        $this->load->model('tool/image');

        $results = $this->model_catalog_product_group->getAllProductGroups();

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])){
                $image = $this->model_tool_image->resize($result['image'], 40, 40);
            }
            else {
                $image = $this->model_tool_image->resize('no_image.png', 40, 40);
            }


            $data['product_groups'][] = array(
                'id' => $result['id'],
                'image' => $image,
                'name' => $result['name'],
                'edit' => $this->url->link('catalog/product_group/edit', 'user_token=' . $this->session->data['user_token'] . '&product_group_id=' . $result['id'] . $url, true)
            );
        }

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->error['warning'])){
            $data['error_warning'] = $this->error['warning'];
        }
        else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])){
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        }
        else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])){
            $data['selected'] = (array)$this->request->post['selected'];
        }
        else {
            $data['selected'] = array();
        }


        $url = '';


        $pagination = new Pagination();

        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/product_group_list', $data));
    }

    public function add ()
    {
        $this->load->language('catalog/product_group');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product_group');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()){
            $this->model_catalog_product_group->addProductGroup($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            $this->response->redirect($this->url->link('catalog/product_group', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    protected function validateForm ()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product_group')){
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if ($this->error && !isset($this->error['warning'])){
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function getForm ()
    {
        $data['text_form'] = !isset($this->request->get['product_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])){
            $data['error_warning'] = $this->error['warning'];
        }
        else {
            $data['error_warning'] = '';
        }


        $url = '';


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product_group', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['product_group_id'])){
            $data['action'] = $this->url->link('catalog/product_group/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        }
        else {
            $data['action'] = $this->url->link('catalog/product_group/edit', 'user_token=' . $this->session->data['user_token'] . '&product_group_id=' . $this->request->get['product_group_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/product_group', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['product_group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
            $product_group_info = $this->model_catalog_product_group->getProductGroup($this->request->get['product_group_id']);
        }


        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        // name

        if (isset($this->request->post['name'])){
            $data['name'] = $this->request->post['name'];
        }
        elseif (!empty($product_group_info)) {
            $data['name'] = $product_group_info['name'];
        }
        else {
            $data['name'] = '';
        }

        // description

        if (isset($this->request->post['description'])){
            $data['description'] = $this->request->post['desctiption'];
        }
        elseif (!empty($product_group_info)) {
            $data['description'] = $product_group_info['dascription'];
        }
        else {
            $data['description'] = '';
        }

        //        // Categories
        $this->load->model('catalog/category');

        if (isset($this->request->post['product_category'])){
            $productGroupCategories = $this->request->post['product_group_category'];
        }
        elseif (isset($this->request->get['product_group_id'])) {
            $productGroupCategories = $this->model_catalog_product_group->getProductGroupCategories($this->request->get['product_group_id']);
        }
        else {
            $productGroupCategories = array();
        }

        $data['product_categories'] = array();



        if ($productGroupCategories!=''){

            foreach ($productGroupCategories as $productGroupCategory) {
                $category_info = $this->model_catalog_category->getCategory($productGroupCategory);

                if ($category_info){
                    $data['product_categories'][] = array(
                        'category_id' => $category_info['category_id'],
                        'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
                    );
                }


            }
        }else{

                $data['product_categories'][] = array(
                    'category_id' => '',
                    'name' => ''
                );

        }


        $this->load->model('tool/image');

        // Image
        if (isset($this->request->post['image'])){
            $data['image'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
            $data['image_url'] = $this->request->post['image'];
        }
        elseif (!empty($product_group_info)) {
            $data['image'] = $this->model_tool_image->resize($product_group_info['image'], 100, 100);
            $data['image_url'] = $product_group_info['image'];
        }
        else {
            $data['image'] = '';
            $data['image_url'] = '';
        }


        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/product_group_form', $data));
    }

    public function edit ()
    {
        $this->load->language('catalog/product_group');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product_group');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()){
            $this->model_catalog_product_group->editProductGroup($this->request->get['product_group_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            $this->response->redirect($this->url->link('catalog/product_group', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete ()
    {
        $this->load->language('catalog/product_group');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product_group');

        if (isset($this->request->post['selected']) && $this->validateDelete()){
            foreach ($this->request->post['selected'] as $product_group_id) {
                $this->model_catalog_product_group->deleteProductGroup($product_group_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';


            $this->response->redirect($this->url->link('catalog/product_group', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function validateDelete ()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product_group')){
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function copy ()
    {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        if (isset($this->request->post['selected']) && $this->validateCopy()){
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_catalog_product->copyProduct($product_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])){
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])){
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])){
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])){
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])){
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])){
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])){
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])){
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function validateCopy ()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product')){
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete ()
    {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])){
            $this->load->model('catalog/product');
            $this->load->model('catalog/option');
            $this->load->model('catalog/product_group');

            if (isset($this->request->get['filter_name'])){
                $filter_name = $this->request->get['filter_name'];
            }
            else {
                $filter_name = '';
            }



            if (isset($this->request->get['limit'])){
                $limit = $this->request->get['limit'];
            }
            else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
//                'filter_model' => $filter_model,
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_catalog_product_group->getProductGroupsByFilter($filter_data);

            foreach ($results as $result) {
                $option_data = array();

//                $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

//                foreach ($product_options as $product_option) {
//                    $option_info = $this->model_catalog_option->getOption($product_option['option_id']);
//
//                    if ($option_info){
//                        $product_option_value_data = array();
//
//                        foreach ($product_option['product_option_value'] as $product_option_value) {
//                            $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);
//
//                            if ($option_value_info){
//                                $product_option_value_data[] = array(
//                                    'product_option_value_id' => $product_option_value['product_option_value_id'],
//                                    'option_value_id' => $product_option_value['option_value_id'],
//                                    'name' => $option_value_info['name'],
//                                    'price' => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
//                                    'price_prefix' => $product_option_value['price_prefix']
//                                );
//                            }
//                        }
//
//                        $option_data[] = array(
//                            'product_option_id' => $product_option['product_option_id'],
//                            'product_option_value' => $product_option_value_data,
//                            'option_id' => $product_option['option_id'],
//                            'name' => $option_info['name'],
//                            'type' => $option_info['type'],
//                            'value' => $product_option['value'],
//                            'required' => $product_option['required']
//                        );
//                    }
//                }

                $json[] = array(
                    'product_group_id' => $result['id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
//                    'model' => $result['model'],
//                    'option' => $option_data,
//                    'price' => $result['price']
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
