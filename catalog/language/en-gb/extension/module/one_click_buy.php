<?php

$_['one_click_buy_button_title'] = 'Купить без регистрации';
$_['one_click_buy_field1_title'] = 'Имя';
$_['one_click_buy_field2_title'] = 'Телефон';
$_['one_click_buy_field3_title'] = 'E-mail';
$_['one_click_buy_field4_title'] = 'Сообщение';
$_['one_click_buy_button_order'] = 'Отправить';
$_['one_click_buy_required_text'] = 'обязательное поле';
$_['one_click_buy_success'] = 'Спасибо за Ваш заказ!<br />Мы свяжемся с Вами в самое ближайшее время.';
$_['one_click_buy_error_required'] = 'Пожалуйста, заполните обязательные поля!';
$_['one_click_buy_error_sending'] = 'Ошибка отправки, пожалуйста, попробуйте повторить позднее!';
