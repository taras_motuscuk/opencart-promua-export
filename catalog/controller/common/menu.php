<?php

class ControllerCommonMenu extends Controller
{
    public function index()
    {
        $this->load->language('common/menu');

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        if (!isset($this->session->data['proffi'])){
            $this->session->data['proffi'] = false;
        }

        if ($this->session->data['proffi'] == 'true'){
            foreach ($categories as $category) {
                if ($category['top']){
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id' => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name' => $child['proffi_name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                            'image' => $this->model_tool_image->resize($child['image'], '200', '200'),
                            'id' => $child['category_id']

                        );
                    }

                    if ($this->request->server['HTTPS']){
                        $server = $this->config->get('config_ssl');
                    }
                    else {
                        $server = $this->config->get('config_url');
                    }
                    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))){
                        $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
                    }
                    else {
                        $data['logo'] = '';
                    }

                    $data['home'] = $this->url->link('common/home');
                    $data['name'] = $this->config->get('config_name');
                    $data['search'] = $this->load->controller('common/search');
                    $data['cart'] = $this->load->controller('common/cart');
                    $data['proffi'] = $this->session->data['proffi'];

                    // Level 1
                    $data['categories'][] = array(
                        'name' => $category['proffi_name'],
                        'image' => $category['image'],
                        'children' => $children_data,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }
            return $this->load->view('common/proffi_menu', $data);

        }
        else {
            foreach ($categories as $category) {
                if ($category['top']){
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id' => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                            'image' => $this->model_tool_image->resize($child['image'], '200', '200'),
                            'id' => $child['category_id']

                        );
                    }

                    if ($this->request->server['HTTPS']){
                        $server = $this->config->get('config_ssl');
                    }
                    else {
                        $server = $this->config->get('config_url');
                    }
                    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))){
                        $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
                    }
                    else {
                        $data['logo'] = '';
                    }

                    $data['home'] = $this->url->link('common/home');
                    $data['name'] = $this->config->get('config_name');
                    $data['search'] = $this->load->controller('common/search');
                    $data['cart'] = $this->load->controller('common/cart');
                    $data['proffi'] = $this->session->data['proffi'];

                    // Level 1
                    $data['categories'][] = array(
                        'name' => $category['name'],
                        'image' => $category['image'],
                        'children' => $children_data,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }

            return $this->load->view('common/menu', $data);
        }

    }

}
