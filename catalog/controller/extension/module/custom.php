<?php

class ControllerExtensionModuleCustom extends Controller
{
    public function index()
    {
        $this->load->model('setting/setting');


        $data['custom'] = array();

        $data['custom']['label'] = $this->model_setting_setting->getSettingValue("module_custom_label");
        $data['custom']['text'] = $this->model_setting_setting->getSettingValue("module_custom_text");
        $data['custom']['button'] = $this->model_setting_setting->getSettingValue("module_custom_button_text");
        $data['custom']['link'] = $this->model_setting_setting->getSettingValue("module_custom_link");

        return $this->load->view('extension/module/custom', $data);
    }
}