<?php

class ControllerExtensionModuleYoutube extends Controller
{
    public function index()
    {


        $data['categories'] = array();

        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);


        $data['product_info'] = $product_info;

        return $this->load->view('extension/module/youtube', $data);
    }
}