<?php

class ControllerExtensionModuleTwitter extends Controller
{
    public function index()
    {

        $this->load->model('setting/setting');


        $data['twitter'] = array();

        $data['twitter']['screen_name'] = $this->model_setting_setting->getSettingValue("module_twitter_screen_name");
        return $this->load->view('extension/module/twitter', $data);
    }
}