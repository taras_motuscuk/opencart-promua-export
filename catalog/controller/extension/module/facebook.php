<?php

class ControllerExtensionModuleFacebook extends Controller
{
    public function index()
    {

        $this->load->model('setting/setting');

        $settings = $this->model_setting_setting->getSettingValue("module_facebook_page_url");
        $data['facebook'] = array();
        $data['facebook']['page_url'] = $settings;

        return $this->load->view('extension/module/facebook', $data);
    }
}