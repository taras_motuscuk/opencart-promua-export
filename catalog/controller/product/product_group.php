<?php

class ControllerProductProductGroup extends Controller
{
    private $error = array();

    public function index ()
    {
        $this->load->language('product/product');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->load->model('catalog/category');

        if (isset($this->request->get['path'])){
            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);
            $product_group_id = $this->request->get['product_group_id'];


            $this->load->model('catalog/product_group');

            $product_group = $this->model_catalog_product_group->getProductGroup($product_group_id);

            $characteristics = $this->model_catalog_product_group->getGroupCharacteristics($product_group_id);


            $data['characteristics'] = $characteristics;
            $this->load->model('tool/image');
            if (isset($product_group[0]['image'])){
                $image = $this->model_tool_image->resize($product_group[0]['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
            }
            else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }

            $data['product_group'] = $product_group[0];
            $data['product_group']['image'] = $image;


            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $data['heading_title'] = $product_group[0]['name'];

        }


        $this->response->setOutput($this->load->view('product/product_group', $data));


    }


}
