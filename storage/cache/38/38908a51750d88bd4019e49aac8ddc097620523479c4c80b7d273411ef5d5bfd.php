<?php

/* extension_liveopencart/related_options/tpl/product_page_script.twig */
class __TwigTemplate_36671f544551478c36ce4ad4d09760cb3761a71f9ba0073f93275ad26858ac25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((((array_key_exists("ro_installed", $context) &&  !(null === (isset($context["ro_installed"]) ? $context["ro_installed"] : null)))) ? ((isset($context["ro_installed"]) ? $context["ro_installed"] : null)) : (false))) {
            // line 2
            echo "\t\t\t\t
\t";
            // line 4
            echo "\t";
            if ((((((array_key_exists("text_select_your", $context) &&  !(null === (isset($context["text_select_your"]) ? $context["text_select_your"] : null)))) ? ((isset($context["text_select_your"]) ? $context["text_select_your"] : null)) : (false)) && (((array_key_exists("options", $context) &&  !(null === (isset($context["options"]) ? $context["options"] : null)))) ? ((isset($context["options"]) ? $context["options"] : null)) : (false))) && twig_test_iterable((isset($context["options"]) ? $context["options"] : null)))) {
                // line 5
                echo "\t\tvar ro_piodd_select_texts = [];
\t\t";
                // line 6
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    // line 7
                    echo "\t\t\t";
                    if (($this->getAttribute($context["option"], "type", array()) == "image")) {
                        // line 8
                        echo "\t\t\t\tro_piodd_select_texts[";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "] = '";
                        echo ((isset($context["text_select_your"]) ? $context["text_select_your"] : null) . $this->getAttribute($context["option"], "name", array()));
                        echo "';
\t\t\t";
                    }
                    // line 10
                    echo "\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 11
                echo "\t";
            }
            // line 12
            echo "\t";
            // line 13
            echo "
\t";
            // line 15
            echo "\t";
            if (((((array_key_exists("ro_data", $context) &&  !(null === (isset($context["ro_data"]) ? $context["ro_data"] : null)))) ? ((isset($context["ro_data"]) ? $context["ro_data"] : null)) : (false)) || ((((array_key_exists("ro_settings", $context) &&  !(null === (isset($context["ro_settings"]) ? $context["ro_settings"] : null)))) ? ((isset($context["ro_settings"]) ? $context["ro_settings"] : null)) : (false)) && ((($this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array(), "any", true, true) &&  !(null === $this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array())))) ? ($this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array())) : (false))))) {
                echo " 
\t\t<style>
\t\t\t.ro_option_disabled { color: #e1e1e1!important; }
\t\t</style>
\t";
            }
            // line 20
            echo "\t";
            // line 21
            echo "
\t\t
\t";
            // line 23
            if (((((array_key_exists("ro_data", $context) &&  !(null === (isset($context["ro_data"]) ? $context["ro_data"] : null)))) ? ((isset($context["ro_data"]) ? $context["ro_data"] : null)) : (false)) || ((($this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array(), "any", true, true) &&  !(null === $this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array())))) ? ($this->getAttribute((isset($context["ro_settings"]) ? $context["ro_settings"] : null), "show_clear_options", array())) : (false)))) {
                // line 24
                echo "\t\t<script type=\"text/javascript\">
\t\t\t
\t\t(function(){
\t\t
\t\t\tvar ro_texts = {};
\t\t\tro_texts['text_ro_clear_options'] \t\t= '";
                // line 29
                echo (((((array_key_exists("text_ro_clear_options", $context) &&  !(null === (isset($context["text_ro_clear_options"]) ? $context["text_ro_clear_options"] : null)))) ? ((isset($context["text_ro_clear_options"]) ? $context["text_ro_clear_options"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["text_ro_clear_options"]) ? $context["text_ro_clear_options"] : null))) : (""));
                echo "';
\t\t\tro_texts['entry_stock_control_error'] = '";
                // line 30
                echo (((((array_key_exists("entry_stock_control_error", $context) &&  !(null === (isset($context["entry_stock_control_error"]) ? $context["entry_stock_control_error"] : null)))) ? ((isset($context["entry_stock_control_error"]) ? $context["entry_stock_control_error"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["entry_stock_control_error"]) ? $context["entry_stock_control_error"] : null))) : (""));
                echo "';
\t\t\tro_texts['text_stock'] \t\t\t\t\t\t\t\t= '";
                // line 31
                echo (((((array_key_exists("text_stock", $context) &&  !(null === (isset($context["text_stock"]) ? $context["text_stock"] : null)))) ? ((isset($context["text_stock"]) ? $context["text_stock"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["text_stock"]) ? $context["text_stock"] : null))) : (""));
                echo "';
\t\t\tro_texts['stock'] \t\t\t\t\t\t\t\t\t\t= '";
                // line 32
                echo (((((array_key_exists("stock", $context) &&  !(null === (isset($context["stock"]) ? $context["stock"] : null)))) ? ((isset($context["stock"]) ? $context["stock"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["stock"]) ? $context["stock"] : null))) : (""));
                echo "';
\t\t\tro_texts['stock_status'] \t\t\t\t\t\t\t= '";
                // line 33
                echo (((((array_key_exists("stock_status", $context) &&  !(null === (isset($context["stock_status"]) ? $context["stock_status"] : null)))) ? ((isset($context["stock_status"]) ? $context["stock_status"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["stock_status"]) ? $context["stock_status"] : null))) : (""));
                echo "';
\t\t\tro_texts['product_model'] \t\t\t\t\t\t= '";
                // line 34
                echo (((((array_key_exists("model", $context) &&  !(null === (isset($context["model"]) ? $context["model"] : null)))) ? ((isset($context["model"]) ? $context["model"] : null)) : (false))) ? (twig_escape_filter($this->env, (isset($context["model"]) ? $context["model"] : null))) : (twig_escape_filter($this->env, (isset($context["ro_product_model"]) ? $context["ro_product_model"] : null))));
                echo "';
\t\t
\t\t\tvar ro_params = {};
\t\t\tro_params['ro_settings'] \t\t= ";
                // line 37
                echo twig_jsonencode_filter((isset($context["ro_settings"]) ? $context["ro_settings"] : null));
                echo ";
\t\t\tro_params['ro_texts'] \t\t\t= ro_texts;
\t\t\tro_params['product_id']\t\t\t= ";
                // line 39
                echo (isset($context["ro_product_id"]) ? $context["ro_product_id"] : null);
                echo ";
\t\t\tro_params['ro_data'] \t\t\t\t= ";
                // line 40
                echo twig_jsonencode_filter((isset($context["ro_data"]) ? $context["ro_data"] : null));
                echo ";
\t\t\tro_params['ro_theme_name'] \t= '";
                // line 41
                echo (isset($context["ro_theme_name"]) ? $context["ro_theme_name"] : null);
                echo "';
\t\t\t";
                // line 42
                if ((((array_key_exists("ros_to_select", $context) &&  !(null === (isset($context["ros_to_select"]) ? $context["ros_to_select"] : null)))) ? ((isset($context["ros_to_select"]) ? $context["ros_to_select"] : null)) : (false))) {
                    // line 43
                    echo "\t\t\t\tro_params['ros_to_select'] = ";
                    echo twig_jsonencode_filter((isset($context["ros_to_select"]) ? $context["ros_to_select"] : null));
                    echo ";
\t\t\t";
                } elseif ((((                // line 44
array_key_exists("ro_filter_name", $context) &&  !(null === (isset($context["ro_filter_name"]) ? $context["ro_filter_name"] : null)))) ? ((isset($context["ro_filter_name"]) ? $context["ro_filter_name"] : null)) : (false))) {
                    // line 45
                    echo "\t\t\t\tro_params['filter_name'] = '";
                    echo (isset($context["ro_filter_name"]) ? $context["ro_filter_name"] : null);
                    echo "';
\t\t\t";
                } elseif ((((                // line 46
array_key_exists("ro_search", $context) &&  !(null === (isset($context["ro_search"]) ? $context["ro_search"] : null)))) ? ((isset($context["ro_search"]) ? $context["ro_search"] : null)) : (false))) {
                    // line 47
                    echo "\t\t\t\tro_params['filter_name'] = '";
                    echo (isset($context["ro_search"]) ? $context["ro_search"] : null);
                    echo "';
\t\t\t";
                }
                // line 49
                echo "\t\t\t";
                if ((((array_key_exists("poip_ov", $context) &&  !(null === (isset($context["poip_ov"]) ? $context["poip_ov"] : null)))) ? ((isset($context["poip_ov"]) ? $context["poip_ov"] : null)) : (false))) {
                    // line 50
                    echo "\t\t\t\tro_params['poip_ov'] = '";
                    echo (isset($context["poip_ov"]) ? $context["poip_ov"] : null);
                    echo "';
\t\t\t";
                }
                // line 52
                echo "\t\t\t
\t\t\tvar \$container_of_options = \$('body');
\t\t\t
\t\t\tvar ro_instance = \$container_of_options.liveopencart_RelatedOptions(ro_params);
\t\t\t
\t\t\tro_instance.common_fn = ro_getCommonFunctions(ro_instance);
\t\t\tif ( typeof(ro_setThemeCommonFunctions) == 'function' ) {
\t\t\t\tro_setThemeCommonFunctions(ro_instance);
\t\t\t}
\t\t\tro_instance.common_fn.initBasic();
\t\t\t\t
\t\t\t";
                // line 64
                echo "\t\t\t";
                if ((((array_key_exists("ro_data", $context) &&  !(null === (isset($context["ro_data"]) ? $context["ro_data"] : null)))) ? ((isset($context["ro_data"]) ? $context["ro_data"] : null)) : (0))) {
                    echo " 
\t\t\t
\t\t\t\tvar spec_fn = ro_getSpecificFunctions(ro_instance);
\t\t\t
\t\t\t\t// to custom
\t\t\t\tro_instance.use_block_options = (\$('a[id^=block-option][option-value]').length || \$('a[id^=block-image-option][option-value]').length || \$('a[id^=color-][optval]').length);
\t\t\t\t
\t\t\t\tro_instance.bind('setOptionValue_after.ro', spec_fn.event_setOptionValue_after);
\t\t\t\tro_instance.bind('init_after.ro', spec_fn.event_init_after);
\t\t\t\tro_instance.bind('setAccessibleOptionValues_select_after.ro', spec_fn.event_setAccessibleOptionValues_select_after);
\t\t\t\tro_instance.bind('setAccessibleOptionValues_radioUncheck_after.ro', spec_fn.event_setAccessibleOptionValues_radioUncheck_after);
\t\t\t\tro_instance.bind('setAccessibleOptionValues_radioToggle_after.ro', spec_fn.event_setAccessibleOptionValues_radioToggle_after);
\t\t\t\tro_instance.bind('setAccessibleOptionValues_radioEnableDisable_after.ro', spec_fn.event_setAccessibleOptionValues_radioEnableDisable_after);
\t\t\t\tro_instance.bind('setSelectedCombination_withAccessControl_after.ro', spec_fn.event_setSelectedCombination_withAccessControl_after);
\t\t\t\tro_instance.bind('controlAccessToValuesOfAllOptions_after.ro', spec_fn.event_controlAccessToValuesOfAllOptions_after);
\t\t\t\t
\t\t\t\tro_instance.spec_fn = spec_fn;
\t\t\t\tif ( typeof(ro_setThemeSpecificFunctions) == 'function' ) {
\t\t\t\t\tro_setThemeSpecificFunctions(ro_instance);
\t\t\t\t}
\t\t\t\t
\t\t\t\tro_instance.custom_radioToggle = spec_fn.custom_radioToggle;
\t\t\t\tro_instance.custom_radioEnableDisable = spec_fn.custom_radioEnableDisable;
\t\t\t\t
\t\t\t\tro_instance.sstore_setOptionsStyles = spec_fn.sstore_setOptionsStyles;
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t";
                }
                // line 93
                echo "\t\t\t";
                // line 94
                echo "\t\t\t
\t\t\tro_instance.initRO();
\t\t
\t\t})();
\t\t
\t\t</script>
\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "extension_liveopencart/related_options/tpl/product_page_script.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 94,  203 => 93,  170 => 64,  157 => 52,  151 => 50,  148 => 49,  142 => 47,  140 => 46,  135 => 45,  133 => 44,  128 => 43,  126 => 42,  122 => 41,  118 => 40,  114 => 39,  109 => 37,  103 => 34,  99 => 33,  95 => 32,  91 => 31,  87 => 30,  83 => 29,  76 => 24,  74 => 23,  70 => 21,  68 => 20,  59 => 15,  56 => 13,  54 => 12,  51 => 11,  45 => 10,  37 => 8,  34 => 7,  30 => 6,  27 => 5,  24 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if ( ro_installed ?? false ) %}*/
/* 				*/
/* 	{# // << Product Image Option DropDown compatibility #}*/
/* 	{% if ( text_select_your ?? false) and (options ?? false) and (options is iterable) %}*/
/* 		var ro_piodd_select_texts = [];*/
/* 		{% for option in options %}*/
/* 			{% if option.type == 'image' %}*/
/* 				ro_piodd_select_texts[{{ option.product_option_id }}] = '{{ text_select_your ~ option.name }}';*/
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 	{% endif %}*/
/* 	{# // >> Product Image Option DropDown compatibility #}*/
/* */
/* 	{# // << the common part and the part for option reset #}*/
/* 	{% if ( ro_data ?? false ) or ( (ro_settings ?? false) and (ro_settings.show_clear_options ?? false) ) %} */
/* 		<style>*/
/* 			.ro_option_disabled { color: #e1e1e1!important; }*/
/* 		</style>*/
/* 	{% endif %}*/
/* 	{# // >> the common part and the part for option reset #}*/
/* */
/* 		*/
/* 	{% if ( ro_data ?? false ) or (ro_settings.show_clear_options ?? false) %}*/
/* 		<script type="text/javascript">*/
/* 			*/
/* 		(function(){*/
/* 		*/
/* 			var ro_texts = {};*/
/* 			ro_texts['text_ro_clear_options'] 		= '{{ (text_ro_clear_options ?? false) ? text_ro_clear_options|e : '' }}';*/
/* 			ro_texts['entry_stock_control_error'] = '{{ (entry_stock_control_error ?? false) ? entry_stock_control_error|e : '' }}';*/
/* 			ro_texts['text_stock'] 								= '{{ (text_stock ?? false) ? text_stock|e : '' }}';*/
/* 			ro_texts['stock'] 										= '{{ (stock ?? false) ? stock|e : '' }}';*/
/* 			ro_texts['stock_status'] 							= '{{ (stock_status ?? false) ? stock_status|e : '' }}';*/
/* 			ro_texts['product_model'] 						= '{{ (model ?? false) ? model|e : ro_product_model|e }}';*/
/* 		*/
/* 			var ro_params = {};*/
/* 			ro_params['ro_settings'] 		= {{ ro_settings|json_encode }};*/
/* 			ro_params['ro_texts'] 			= ro_texts;*/
/* 			ro_params['product_id']			= {{ ro_product_id }};*/
/* 			ro_params['ro_data'] 				= {{ ro_data|json_encode }};*/
/* 			ro_params['ro_theme_name'] 	= '{{ ro_theme_name }}';*/
/* 			{% if ( ros_to_select ?? false ) %}*/
/* 				ro_params['ros_to_select'] = {{ ros_to_select|json_encode }};*/
/* 			{% elseif ( ro_filter_name ?? false ) %}*/
/* 				ro_params['filter_name'] = '{{ ro_filter_name }}';*/
/* 			{% elseif ( ro_search ?? false ) %}*/
/* 				ro_params['filter_name'] = '{{ ro_search }}';*/
/* 			{% endif %}*/
/* 			{% if ( poip_ov ?? false) %}*/
/* 				ro_params['poip_ov'] = '{{ poip_ov }}';*/
/* 			{% endif %}*/
/* 			*/
/* 			var $container_of_options = $('body');*/
/* 			*/
/* 			var ro_instance = $container_of_options.liveopencart_RelatedOptions(ro_params);*/
/* 			*/
/* 			ro_instance.common_fn = ro_getCommonFunctions(ro_instance);*/
/* 			if ( typeof(ro_setThemeCommonFunctions) == 'function' ) {*/
/* 				ro_setThemeCommonFunctions(ro_instance);*/
/* 			}*/
/* 			ro_instance.common_fn.initBasic();*/
/* 				*/
/* 			{# // << the part when the product has related options #}*/
/* 			{% if ( ro_data ?? 0) %} */
/* 			*/
/* 				var spec_fn = ro_getSpecificFunctions(ro_instance);*/
/* 			*/
/* 				// to custom*/
/* 				ro_instance.use_block_options = ($('a[id^=block-option][option-value]').length || $('a[id^=block-image-option][option-value]').length || $('a[id^=color-][optval]').length);*/
/* 				*/
/* 				ro_instance.bind('setOptionValue_after.ro', spec_fn.event_setOptionValue_after);*/
/* 				ro_instance.bind('init_after.ro', spec_fn.event_init_after);*/
/* 				ro_instance.bind('setAccessibleOptionValues_select_after.ro', spec_fn.event_setAccessibleOptionValues_select_after);*/
/* 				ro_instance.bind('setAccessibleOptionValues_radioUncheck_after.ro', spec_fn.event_setAccessibleOptionValues_radioUncheck_after);*/
/* 				ro_instance.bind('setAccessibleOptionValues_radioToggle_after.ro', spec_fn.event_setAccessibleOptionValues_radioToggle_after);*/
/* 				ro_instance.bind('setAccessibleOptionValues_radioEnableDisable_after.ro', spec_fn.event_setAccessibleOptionValues_radioEnableDisable_after);*/
/* 				ro_instance.bind('setSelectedCombination_withAccessControl_after.ro', spec_fn.event_setSelectedCombination_withAccessControl_after);*/
/* 				ro_instance.bind('controlAccessToValuesOfAllOptions_after.ro', spec_fn.event_controlAccessToValuesOfAllOptions_after);*/
/* 				*/
/* 				ro_instance.spec_fn = spec_fn;*/
/* 				if ( typeof(ro_setThemeSpecificFunctions) == 'function' ) {*/
/* 					ro_setThemeSpecificFunctions(ro_instance);*/
/* 				}*/
/* 				*/
/* 				ro_instance.custom_radioToggle = spec_fn.custom_radioToggle;*/
/* 				ro_instance.custom_radioEnableDisable = spec_fn.custom_radioEnableDisable;*/
/* 				*/
/* 				ro_instance.sstore_setOptionsStyles = spec_fn.sstore_setOptionsStyles;*/
/* 				*/
/* 				*/
/* 				*/
/* 			{% endif %}*/
/* 			{# // >> the part when the product has related options #}*/
/* 			*/
/* 			ro_instance.initRO();*/
/* 		*/
/* 		})();*/
/* 		*/
/* 		</script>*/
/* 	{% endif %}*/
/* {% endif %}*/
